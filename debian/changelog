ding (1.9-9) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.7.2.
  * Update years of packaging copyright.
  * Drop unneeded version constraints from (build) dependencies.
  * 21_de-en-devel: Update to devel version (2025-02-28) of de-en dict.

 -- Roland Rosenfeld <roland@debian.org>  Sat, 01 Mar 2025 13:50:11 +0100

ding (1.9-8) unstable; urgency=medium

  * Update copyright in debian/rules.
  * 21_de-en-devel: Update to devel version (2024-10-25) of de-en dict.
  * Update to Standards-Version 4.7.0 (no changes).
  * upstream/metadata: webservice moved, reference deleted.
  * Update debian/salsa-ci.yml.
  * debian/control: glimpse replaced agrep, tre-agrep is no longer
    preferred, upstream site has changed.

 -- Roland Rosenfeld <roland@debian.org>  Sat, 26 Oct 2024 11:22:12 +0200

ding (1.9-7) unstable; urgency=medium

  * 21_de-en-devel: Update to devel version (2023-04-13) of de-en dict.
  * Recommends agrep | glimpse now instead of the slower tre-agrep
    (Closes: #1051544).

 -- Roland Rosenfeld <roland@debian.org>  Wed, 13 Sep 2023 17:42:51 +0200

ding (1.9-6) unstable; urgency=medium

  * Remove bsdmainutils alternative from Build-Depends.
  * 21_de-en-devel: Update to devel version (2023-01-30) of de-en
    dictionary.  This fixes typo in "bone tumor" (Closes: #1029963).

 -- Roland Rosenfeld <roland@debian.org>  Wed, 01 Feb 2023 17:49:56 +0100

ding (1.9-5) unstable; urgency=medium

  * Update to Standards-Version 4.6.2 (no changes).
  * 21_de-en-devel: Update to devel version (2022-12-14) of de-en
    dictionary.

 -- Roland Rosenfeld <roland@debian.org>  Fri, 30 Dec 2022 13:43:16 +0100

ding (1.9-4) unstable; urgency=medium

  [ Roland Rosenfeld ]
  * debian/control: update Build-Depends for cross builds.
  * Update to Standards-Version 4.6.1 (no changes).
  * 21_de-en-devel: Import devel version (2022-09-07) of de-en dictionary.
  * Update dictd-dictionary description.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends-Indep: Drop versioned constraint on dictfmt.
    + ding: Drop versioned constraint on trans-de-en in Recommends.
    + ding: Drop versioned constraint on trans-de-en in Breaks.

 -- Roland Rosenfeld <roland@debian.org>  Fri, 09 Dec 2022 15:56:01 +0100

ding (1.9-3) unstable; urgency=medium

  [ Roland Rosenfeld ]
  * Update to Standards-Version 4.6.0 (no changes).
  * Use "command -v" instead of deprecated "which" in maintainer scripts.

  [ Debian Janitor ]
  * Remove duplicate values for fields Reference, Reference in
    debian/upstream/metadata.
  * Remove constraints unnecessary since buster:
    + dict-de-en: Drop versioned constraint on dictd in Breaks.

 -- Roland Rosenfeld <roland@debian.org>  Thu, 26 Aug 2021 20:50:24 +0200

ding (1.9-2) unstable; urgency=medium

  * Recommends glimpse (>=4.18.7-6) as an alternative to agrep now.
  * 20_tre-agrep: prefer agrep over tre-agrep.

 -- Roland Rosenfeld <roland@debian.org>  Sun, 17 Jan 2021 12:46:55 +0100

ding (1.9-1) unstable; urgency=medium

  * New upstream version 1.9.
  * Adapt all patches to new upstream version.
  * The following patches are now incorporated upstream:
    01_ispell_dirname, 07_8bit_aspell, 08_de-en-devel, 11_desktop_file,
    12_dont_set_locale, 14_no_strip_UTF-8, 16_egrep-a.
  * Update standards version to 4.5.1, no changes needed.

 -- Roland Rosenfeld <roland@debian.org>  Sun, 10 Jan 2021 18:53:55 +0100

ding (1.8.1-10) unstable; urgency=medium

  * Recode documentation to UTF-8.
  * 08_de-en-devel: Import devel version (2020-08-27) of de-en dictionary.
    This supersedes: 09_seperation, 13_Gie0erei, 17_Behaelter,
    18_changeable, 21_Primarschule, 22_Stegreif, 23_Verlaengerung,
    24_vornehmen, 25_management, 26_Mannschaftskollegin (Closes: #972660).
  * Add debian/upstream/metadata.
  * Add Forwarded headers to all patches.
  * Add lintian-overrides: upstream-metadata-missing-repository, since no
    repository exists upstream.
  * Write dictionary version to dictd dictionaries and adapt tests/dictd
    accordingly.
  * Add more typos to tests/typos.

 -- Roland Rosenfeld <roland@debian.org>  Fri, 23 Oct 2020 14:04:59 +0200

ding (1.8.1-9) unstable; urgency=medium

  * 26_Mannschaftskollegin: Fix two typos in Mannschaftskollegin(nen)
    (Closes: #939603).
  * Update standards version to 4.5.0, no changes needed.
  * Rename gitlab-ci.yml to salsa-ci.yml.
  * Update debian/watch to version=4 and optimize regex.
  * Build-Depends-Indep: bsdextrautils (>= 2.35.2-3)|bsdmainutils (<<12.1.1)
    because col was moved (Closes: #963427).
  * Update to debhelper 13.

 -- Roland Rosenfeld <roland@debian.org>  Sun, 21 Jun 2020 23:38:56 +0200

ding (1.8.1-8) unstable; urgency=medium

  * 24_vornehmen: Fix typo in "vornehmen" (Closes: #930688).
  * 11_desktop_file: More optimization of ding.desktop.
  * Upgrade to Standards-Version 4.4.0 (no changes).
  * Add debian/tests/typos to check for known (former) typos in dictionary.
  * 25_management: Fix typo in "management" (missing 'e').

 -- Roland Rosenfeld <roland@debian.org>  Fri, 12 Jul 2019 14:02:53 +0200

ding (1.8.1-7) unstable; urgency=medium

  * Add buildlog to gitlab-ci test pipeline.
  * 23_Verlaengerung: Fix typo in Verlängerungsschnüre (Closes: #920808).
  * Build-depend on debhelper-compat (= 12) instead of using d/compat.
  * Generate xpm icons and a 64x64 icon on build time using imagemagick.
  * d/gitlab-ci.yml stripped down using pipline-jobs.yml

 -- Roland Rosenfeld <roland@debian.org>  Fri, 01 Feb 2019 22:43:30 +0100

ding (1.8.1-6) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP

  [ Roland Rosenfeld ]
  * d/tests: Fix leftover ADTTMP.
  * Add salsa CI pipeline in debian/gitlab-ci.yml.
  * Upgrade to Standards-Version 4.3.0 (no changes).
  * Update debhelper to v12.
  * 22_Stegreif: Fix typo in Stegreif (Closes: #918335).
  * Change download-URL to https://

 -- Roland Rosenfeld <roland@debian.org>  Sat, 05 Jan 2019 13:08:26 +0100

ding (1.8.1-5) unstable; urgency=medium

  * Remove shellcheck warnings from test scripts.
  * Remove dict-de-en.preinst, which is no longer used.
  * Upgrade to Standards-Version 4.2.1 (Declare Rules-Requires-Root: no).
  * Avoid duplicate trap execution in debian/tests.  Thanks to Balint Reczey.
  * 21_Primarschule: Fix typo in Primarschule (Closes: #907874).
  * Switch from http:// to https:// on several pages.

 -- Roland Rosenfeld <roland@debian.org>  Mon, 03 Sep 2018 19:09:33 +0200

ding (1.8.1-4) unstable; urgency=medium

  * Add Vcs-headers pointing to salsa.debian.org.
  * 20_tre-agrep: Add support for tre-agrep as an agrep alternative.
  * Upgrade to Standards-Version 4.1.4 (no changes).
  * Add debian testsuite.
  * Set MAN_DISABLE_SECCOMP=1 for man page conversion.
  * Automatically update upstream version in dict dictionary title.

 -- Roland Rosenfeld <roland@debian.org>  Sun, 29 Apr 2018 22:40:03 +0200

ding (1.8.1-3) unstable; urgency=medium

  * Update to Standards-Version 4.1.3 (no changes).
  * Update debhelper to v11.
  * Use en_US/de_DE as dictionaries for hunspell/aspell instead of
    british/ngerman and prefer aspell over ispell (Closes: #889758).
  * Suggests hunspell dictionaries instead of ispell.

 -- Roland Rosenfeld <roland@debian.org>  Thu, 08 Feb 2018 10:51:58 +0100

ding (1.8.1-2) unstable; urgency=medium

  * 18_changeable: Fix typo in changeable (Closes: #882911).
  * Update to Standards-Version 4.1.2
    - change copyrights-format to https.
  * Tag ding and trans-de-en Multi-Arch: foreign.
  * Make DEP3 comments more gbp pq compatible.

 -- Roland Rosenfeld <roland@debian.org>  Fri, 22 Dec 2017 14:20:20 +0100

ding (1.8.1-1) unstable; urgency=medium

  * New upstream version 1.8.1 (Closes: #810935).
  * Update all patches to new version.
  * Remove 05_manpage, 08_foreseeable, 15_Korollar, which are included upstream.
  * Update html documentation.
  * Change debian/watch from ftp:// to http://

 -- Roland Rosenfeld <roland@debian.org>  Fri, 09 Sep 2016 10:12:54 +0200

ding (1.8-6) unstable; urgency=medium

  * 16_egrep-a: Use "egrep -a" to work around problems with grep seeing a
    wrong charset as binary (solves Ubuntu #1585107).
  * 17_Behaelter: Fix typo: s/Behalter/Behälter/ s/äußester/äußerster/ and
    add parentheses (Closes: #828045).

 -- Roland Rosenfeld <roland@debian.org>  Sat, 25 Jun 2016 13:09:07 +0200

ding (1.8-5) unstable; urgency=medium

  * 15_Korollar: Fix gender of Korollar (Closes: #819955).
  * Update to Standards-Version 3.9.8 (no changes).

 -- Roland Rosenfeld <roland@debian.org>  Fri, 08 Apr 2016 16:15:59 +0200

ding (1.8-4) unstable; urgency=medium

  * 14_no_strip_UTF-8: Do not strip off .UTF-8 from LANG variable, since
    this causes problems with grep (Closes: #815065).
  * Update to Standards-Version 3.9.7 (no changes).
  * Add -p to QUILT_DIFF_OPTS.
  * Fix image path in html documentation (Closes: #812180).
  * Run wrap-and-sort.
  * Move upstream documentation to debian/html in source package.
  * Remove debian/conv.py, which is no longer used.
  * Update debian/copyright.
  * Install ding.png to 32x32 icons directory.

 -- Roland Rosenfeld <roland@debian.org>  Fri, 19 Feb 2016 11:20:59 +0100

ding (1.8-3) unstable; urgency=medium

  * Optimize text version of man page for reproducibility.

 -- Roland Rosenfeld <roland@debian.org>  Sat, 17 Oct 2015 12:45:12 +0200

ding (1.8-2) unstable; urgency=medium

  * Set LC_ALL=C.UTF-8 when creating text version of man page to make this
    reproducible.
  * Avoid paths in postinst/rm scripts.
  * Remove debian/menu, since a desktop file is present.

 -- Roland Rosenfeld <roland@debian.org>  Wed, 14 Oct 2015 08:34:49 +0200

ding (1.8-1) unstable; urgency=low

  * New upstream version 1.8.
  * Update all patches.
  * Remove 10_Aour (included upstream).
  * Update to Standards-Version 3.9.6 (no changes).
  * Convert debian/copyright to DEP5.
  * 13_Gie0erei: Fix typo: s/Gie0erei/Gießerei/ (Closes: #758540).
  * Depend on tk instead of tk8.4|wish (Closes: #781800).
  * Run dictfmrt --without-time as first step to reproducible builds.
  * Complete rewrite of debian/rules based on dh.

 -- Roland Rosenfeld <roland@debian.org>  Sun, 26 Apr 2015 09:54:55 +0200

ding (1.7-3) unstable; urgency=low

  * 08_foreseeable: add missing "e" to "foreseeable" (Closes: #685809).
  * 09_seperation: typo: s/seperation/separation/ (Closes: #719823).
  * 10_Aour: typo: s/Aour/Your/ (Closes: #740878).
  * 11_desktop_file: Fixed installation of ding.desktop file. Thanks to
    Oliver Sauder (Closes: #744276).
  * Remove Encoding from desktop file, since UTF-8 is now the default.
  * Add keywords to desctop file.
  * Update to Standards-Version 3.9.5 (no changes).
  * 12_dont_set_locale: The explicit definition of LC_TYPE to de_DE.UTF8
    causes problems if the Linux installation uses another locale setting.
    (Closes: #492458).
  * Optimize dict-de-en Suggests for dict-client and dict-server.

 -- Roland Rosenfeld <roland@debian.org>  Sun, 11 May 2014 15:00:52 +0200

ding (1.7-2) unstable; urgency=low

  * Remove deprecated dpatch and upgrade to packaging format "3.0 quilt".
  * Update to Standards-Version to 3.9.3 and debhelper to 9.
  * Add build-arch and build-indep targets; use dh_prep in rules file.
  * Fix copyright-refers-to-symlink-license (Lintian).
  * Fix spelling-error-in-copyright (Lintian).
  * Fix description-synopsis-starts-with-article (Lintian).
  * Thanks to Jari Aalto for providing a patch for the above points.
    (Closes: #668542).
  * Get rid of uuencoded images, they are handled by new 3.0 quilt format
    now.

 -- Roland Rosenfeld <roland@debian.org>  Tue, 12 Jun 2012 18:23:21 +0200

ding (1.7-1) unstable; urgency=low

  * New upstream version 1.7.
    - s/succesful/successful/ (Closes: #596376).
  * Adapt all patches to new version.
  * Remove 08_duplicate_colons, and 09_wronghash, and 10_management.
  * Update debian/*.html documentation from website.
  * Add debian/source/format 1.0.
  * Update to Standards-Version 3.9.1
    - Change versioned Conflicts to Breaks.

 -- Roland Rosenfeld <roland@debian.org>  Wed, 02 Mar 2011 22:28:21 +0100

ding (1.6-2) unstable; urgency=low

  * 09_wronghash: Remove superfluous # from dictionary (Closes: #459351).
  * 10_management: Fix typo in "Management" (Closes: #538050).
  * Add ${misc:Depends} dependency to all packages.
  * Upgrade to Standards-Version 3.8.4 (no changes).

 -- Roland Rosenfeld <roland@debian.org>  Fri, 11 Jun 2010 16:35:30 +0200

ding (1.6-1) unstable; urgency=low

  * New upstream version 1.6.
  * Adapt all patches to new version.
  * Remove 06_verantwortlich, now incorporated upstream.
  * Added debian/README.source (from dpatch package) to explain how dpatch
    works.
  * Remove overrides.lintian (no longer necessary)
  * Change doc-base section to "Text".
  * Upgrade to Standards-Version 3.8.3 (no changes).

 -- Roland Rosenfeld <roland@debian.org>  Sun, 06 Dec 2009 14:54:01 +0100

ding (1.5-3) unstable; urgency=low

  * Upgrade to Standards-Version 3.7.3:
    - Make "Homepage" a real header in debian/control.
    - Change menu section from Apps/Text to Applications/Text.
  * 08_duplicate_colons.dpatch: Fix duplicate colons in dictionary
    (Closes: #448842).

 -- Roland Rosenfeld <roland@debian.org>  Sun, 23 Dec 2007 19:01:03 +0100

ding (1.5-2) unstable; urgency=low

  * dict-de-en now provides dictd-dictionary (see: #413575).
  * 07_8bit_aspell: Do not replace Umlauts by a" etc. for aspell, but only
    for ispell, because aspell needs 8bit chars (Closes: #359243).
  * Added the plugin files for using the dict formatted dictionaries with
    the OpenDict dictionary program.  For that added files
    debian/dict-de-en.{dirs, links}, english-german-conf.xml,
    german-english-conf.xml and accordingly added the new statements in
    the targets 'install' of the debian/rules.  Thanks to Kęstutis
    Biliūnas <kebil@kaunas.init.lt> for providing these (Closes: #426597).
  * Added the packages dict | opendict | kdict to Suggests.
  * Moved dictd | dict-server to Suggests like other dict-freedict
    packages do (because OpenDict can use these dictionaries without
    dict-server running).
  * Added serpento as another alternative to dictd.  It's already included
    in dict-server, but in Suggests this may be easier to read for users.
  * Removed dependency on debconf.  This was forgotten to do in the
    revision 1.4-3.

 -- Roland Rosenfeld <roland@debian.org>  Sun, 29 Jul 2007 11:20:32 +0200

ding (1.5-1) unstable; urgency=low

  * New upstream version 1.5.
    * This fixes the word "hanggeahbt" (Closes: #408111).
  * Migrate all patches to dpatch mechanism.
  * 06_verantwortliche.dpatch: Fix syntax of "Verantwortliche" in
    dictionary.
  * Add lintian overrides, because /usr/bin/ding isn't a shell script but
    a wish script and lintian complains about that.
  * Add debian/watch file.

 -- Roland Rosenfeld <roland@debian.org>  Sat, 19 May 2007 18:16:30 +0200

ding (1.4-4) unstable; urgency=low

  * Build-Depends instead of Build-Depends-Indep on debhelper.
  * Upgrade to debhelper v5.
  * Upgrade to Standards-Version 3.7.2 (no changes)

 -- Roland Rosenfeld <roland@debian.org>  Fri, 30 Jun 2006 23:16:34 +0200

ding (1.4-3) unstable; urgency=low

  * Change menu section from Apps/Tools to Apps/Text (Closes: #344130).
  * Remove recode from build-depens, because it is no longer used.
  * Update FSF address in copyright file.
  * Use dictfmt --utf8 instead of all the old locale hack and build-depend
    on dictfmt >= 1.10.1.
  * Change dictd dependency to dictd (>= 1.10.1), which supports utf8
    without tweeking the dictd configuration.
  * This implies, that there is no need to modify /etc/default/dictd in
    dict-de-en.postinst.
  * This implies that no debconf template is needed any more
    (Closes: Closes: #336073, #340972).
  * Removed build-dependency on po-debconf.

 -- Roland Rosenfeld <roland@debian.org>  Sun, 25 Dec 2005 20:42:21 +0100

ding (1.4-2) unstable; urgency=low

  * Capitalize "Sie" in "Bitte warten sie einen Augenblick!"
    (Closes: #285453, #283787).
  * Change package description to mention "graphical" and "Tk"
    (Closes: #311566).
  * Depend on debconf (>= 0.5) | debconf-2.0.
  * Upgrade to policy 3.6.2 (no changes).
  * Add Vietnamese debconf template (Closes: #313533).
  * Add Czech debconf template (Closes: #315833).
  * s/unsympatisch/unsympathisch/ (Closes: #324654).
  * ding now recommends trans-de-en (>= 1.4) | translation-dictionary and
    conflicts trans-de-en (<< 1.4) because the de-en dictionary was
    iso-8859-1 encoded up to version 1.3 while the new ding package
    expects an utf-8 encoded dictionary. Thanks to Peter Roosen for
    mentioning this.
  * Use cat(1) as the man(1) pager in debian/rules otherwise pbuilder is
    unlucky if less(1) is not installed and creates an empty page.

 -- Roland Rosenfeld <roland@debian.org>  Mon, 26 Sep 2005 21:29:24 +0200

ding (1.4-1) unstable; urgency=low

  * New upstream version 1.4.
    - This is now UTF-8 clean (Closes: #177915, #272701).
    - "a "o "u "s are now correctly converted to ä ö ü ß (Closes: 272963).
  * Change default ispell dictionaries from english to british and from
    german to ngerman and suggest ingerman and ibritish dictionaries.
  * Add Japanese debconf template (Closes: #276807)
  * Fix quoting of [ia]spell input to make aspell work again
    (Closes: #299176).

 -- Roland Rosenfeld <roland@debian.org>  Thu,  5 May 2005 16:47:59 +0200

ding (1.3-5) unstable; urgency=low

  * Redirect output of invoke-rc.d in postrm to stderr instead of stdout,
    otherwise debconf fails on purging the package (Closes: #245999).

 -- Roland Rosenfeld <roland@debian.org>  Sun,  8 Aug 2004 23:48:50 +0200

ding (1.3-4) unstable; urgency=low

  * Add debconf de.po (Closes: #252328).
  * Add debconf nl.po (Closes: #250229).
  * s/Belgien/Belgium/ (in English) (Closes: #256653).
  * chmod 755 debian/conv.pl (Closes: #262396).
  * Remove setting of LANG=de_DE at the beginning, which otherwise makes
    it impossible to use the English version of ding (Closes: #248462).

 -- Roland Rosenfeld <roland@debian.org>  Sat,  7 Aug 2004 17:01:35 +0200

ding (1.3-3) unstable; urgency=medium

  * dict-de-en.postinst: ignore entries in locale.gen, if they start with
    a '#', because these are comments (Closes: #246979).
  * Convert debian/changelog from latin1 to utf-8.
  * Quote "X11" in debian/menu.

 -- Roland Rosenfeld <roland@debian.org>  Sun,  2 May 2004 21:27:15 +0200

ding (1.3-2) unstable; urgency=low

  * Update debconf fr.po (Closes: #237095).

 -- Roland Rosenfeld <roland@debian.org>  Sun, 25 Apr 2004 11:34:16 +0200

ding (1.3-1) unstable; urgency=low

  * New upstream version 1.3.
  * New version supports quoting of input special chars (Closes: #224335).
  * Add French translation of debconf templates (Closes: #232096).
  * s/novell/novel/ (Closes: #233814).

 -- Roland Rosenfeld <roland@debian.org>  Fri, 20 Feb 2004 19:34:19 +0100

ding (1.2-7) unstable; urgency=low

  * Some more corrections of conv.pl to support semicolon in parentheses
    and braces and to avoid empty index words in the dictd dictionaries.
    This continues to close #222824.

 -- Roland Rosenfeld <roland@debian.org>  Sun,  8 Feb 2004 00:49:12 +0100

ding (1.2-6) unstable; urgency=low

  * dict-de-en no longer depends on dict.
  * Use dictd as primary alternative to dict-server.
  * Add dict-de-en.preinst to remove the broken
    'DICTD_ARGS=" --locale=de_DE.UTF-8"' from /etc/default/dictd, which
    was introduced in 1.2-5.
  * Inform the user, if no UTF-8 locale is available via debconf.
  * Ask the user, whether he wants to add --locale=xx_YY.UTF-8 to his
    /etc/defaults/dictd via debconf before changing this file
    (Closes: #230640).
  * dict-de-en.postrm: only restart dictd on purge/remote.

 -- Roland Rosenfeld <roland@debian.org>  Sun,  1 Feb 2004 22:18:20 +0100

ding (1.2-5) unstable; urgency=low

  * s/summarieses/summarises/, s/formating/formatting/,
    s/license/to license/ (Closes: #229463).
  * Remove duplicated "::" from dictionary (line "in terms of").
  * Build dict-de-en package from this source instead of providing
    a separate source package which build-depends from this package
    (Thanks to Andreas Tille <tille@debian.org> for providing this).
  * Added "Homepage:" to long decription according to developers
    reference.
  * Added Build-Depends from dict-de-en.
  * dict-de-en depends from dict-server (instead of dictd)
    (Closes: #227513).
  * Recode dictd dictionary to UTF-8 (Closes: #217210).
  * Rewrite of conv.py in Perl.
  * Split alternatives ("|") for dictd usage into multiple entries.
  * Add invoke-rc.d support to dict-de-en postinst/postrm scripts.
  * Missing " : " added to some abbreviations.
  * Add support for abbreviations (defined by " : ") (Closes: #222824).
  * Add "Beschluss {m} | Beschlüsse {pl} :: resolution | resolutions"
    (Closes: 230156).

 -- Roland Rosenfeld <roland@debian.org>  Sat, 31 Jan 2004 09:29:47 +0100

ding (1.2-4) unstable; urgency=low

  * Distinguish between summarize [Am.] and summariese [Br.] (Closes: #212140).
  * s/extraparliarnentary/extraparliamentary/ (Closes: #211700).
  * s/formated/formatted/, s/lizensieren/lizenzieren/,
    s/lizensiert/lizenziert/, s/licensee/license/.  Thanks to Jens Seidel
    <tux-master@web.de> (Closes: #229260).

 -- Roland Rosenfeld <roland@debian.org>  Sat, 24 Jan 2004 16:39:47 +0100

ding (1.2-3) unstable; urgency=low

  * s/lizensieren/lizenzieren/
  * Remove trailing "Y" from "monthly salariesY" (Closes: #201689).
  * s/Ausschliesslichkeit/Ausschließlichkeit/,
    s/einschliessen/einschließen/, s/Systemresourcen/Systemressourcen/
    (Closes: #203594, #205352).
  * Upgrade to Standards-Version 3.6.1 (no changes).
  * Upgrade to debhelper 4:
    - use debian/compat 4.
    - build-depend on debhelper >=4
    - update debian/rules based on the debhelper example
  * Add icon16x16 and icon32x32 entries to debian/menu.
  * Move xpm icons from /usr/X11R6/include/X11/pixmaps to
    /usr/share/pixmaps.

 -- Roland Rosenfeld <roland@debian.org>  Tue,  9 Sep 2003 21:25:10 +0200

ding (1.2-2) unstable; urgency=low

  * s/cooton-wool/cotton-wool/ (Closes: #162182).
  * Do no longer suggest glimpse as an alternative to agrep (Closes: #110197).
  * Use tk8.4 instead of tk8.3 as the default wish provides.
  * Upgrade to policy 3.5.7 (no changes).
  * Updated package descriptions and copyright.
  * Mention problem with "search on new text selection" and lost
    characters when using tk8.0 in README.Debian (Closes: #159089).
  * Modify urlOpen() function to support several browsers (not only
    mozilla and netscape) and to correctly start an x-terminal-emulator
    for ASCII browsers like w3m, links and lynx (Closes: #157007).

 -- Roland Rosenfeld <roland@debian.org>  Sun, 20 Oct 2002 13:22:55 +0200

ding (1.2-1) unstable; urgency=low

  * New upstream version 1.2.
  * Manpage is now available in upstream package.

 -- Roland Rosenfeld <roland@debian.org>  Thu,  1 Aug 2002 17:58:35 +0200

ding (1.1-3) unstable; urgency=low

  * Correct Link to ding-1.1.gif in index.html.
  * Use -w (complete words) option on agrep, when "best match" is enabled,
    too (upstream disables -w when "best match" is enabled, which seems to
    be a bad idea) (Closes: #127317).
  * Update to debhelper >=2.2.0 and adapt debian/rules accordingly.

 -- Roland Rosenfeld <roland@debian.org>  Sun,  6 Jan 2002 15:05:57 +0100

ding (1.1-2) unstable; urgency=low

  * Upgrade to Standards-Version 3.5.5 (no changes).
  * Use debhelper v2 now (minor changes to debian/rules).
  * Suggest agrep | glimpse, because there's a separate package agrep now.
  * Depend on tk8.3 | wish instead of tk8.2 | wish now.

 -- Roland Rosenfeld <roland@debian.org>  Fri, 20 Jul 2001 20:24:50 +0200

ding (1.1-1) unstable; urgency=low

  * New upstream version:
    - many new features (see upstream Changelog)
    - updated dictionary
  * Removed printbuffer.c (this functionality is implemented in ding now).
  * Updated manpage to new ding features.

 -- Roland Rosenfeld <roland@debian.org>  Mon, 29 May 2000 01:30:58 +0200

ding (1.0-6) frozen unstable; urgency=low

  * debian/control: Added alternative "tk8.2" for "wish" fixing tasksel
    (Closes: #64604).
  * This closes an important bug, so it should go into frozen.
  * Thanks to Torsten Landschoff <torsten@debian.org> for providing the
    patch.

 -- Roland Rosenfeld <roland@debian.org>  Thu, 25 May 2000 14:51:11 +0200

ding (1.0-5) frozen unstable; urgency=low

  * Use "winfo rgb" to convert color to RGB, otherwise we run into
    trouble, if someone uses a background color (for example in
    Xresources) which doesn't match #rrggbb format (Closes: #62079).
  * This should go into frozen, because it closes an important bug.

 -- Roland Rosenfeld <roland@debian.org>  Mon, 10 Apr 2000 00:37:40 +0200

ding (1.0-4) frozen unstable; urgency=low

  * Do no longer depend on "glimpse | grep" but suggest "glimpse"
    (Closes: #60854)
  * This doesn't change any of the internals of the package, so it can go
    into frozen.

 -- Roland Rosenfeld <roland@debian.org>  Wed, 22 Mar 2000 12:01:42 +0100

ding (1.0-3) unstable; urgency=low

  * Optimized menu file (added hints, corrected section, use 32x32 icon).
  * Upgrade policy to 3.1.1: Add Build-Depends.

 -- Roland Rosenfeld <roland@debian.org>  Sun, 12 Dec 1999 18:16:08 +0100

ding (1.0-2) unstable; urgency=low

  * Use local copy of perl script uudecode, because perl-5.005-doc is not
    always available.
  * Upgrade policy from 2.5.0 to 3.0.1:
    - GPL now in /usr/share/common-licenses/
    - FSSTND -> FHS
  * Depend on wish instead of tk8.0, which should be much more flexible.
  * Start wish instead of wish8.0, because ding also works with tk8.2.

 -- Roland Rosenfeld <roland@debian.org>  Fri, 24 Sep 1999 18:59:39 +0200

ding (1.0-1) unstable; urgency=low

  * Initial Release.
  * Added feature to enter a word/phrase via command line. See
    README.Debian for more information about this.
  * Added documentation from web page.
  * Renamed dictionary to /usr/share/trans/de-en.
  * Introduced virtual package translation-dictionary.

 -- Roland Rosenfeld <roland@debian.org>  Fri,  4 Jun 1999 23:24:56 +0200
